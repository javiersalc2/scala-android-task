package com.chucho.wireapp.domain

/**
  * Created by chucho on 12/14/16.
  */

import org.scalatest.Matchers._
import org.scalatest._


class UtilSpec extends FlatSpec {

  behavior of "utils"

  it should "message object to json message object" in {
    val message = Message("5454", 14452, "http://hola.com")
    val obj = Util.toJson(message)
    message.text shouldEqual obj.getString("text")
  }

  it should "take a list of messages, parse to jsonarray" in {
    val messages = List(Message("5454", 14452, "http://hola.com"),
      Message("5455", 14452, "http://hola2.com"),
      Message("5456", 14452, "bye"))
    val jsonArray = Util.toJsonArray(messages map (Util.toJson(_)))
    val id = jsonArray.getJSONObject(messages.length - 1).getString("id")
    messages.last.id shouldEqual id
  }

  it should "be an instance of Message" in {

    val jsonString = """[{
                       |    "id": "78deb1e2-c22e-478f-8b53-d0ab1146bc17",
                       |    "time": 1463366414205,
                       |    "text": "https:\/\/unsplash.com\/photos\/IoCWq07GaG4\/download"
                       |  }]""".stripMargin

    val jsons = Util.parseResponse(jsonString)
    val msg = Util.msgFromJson(jsons.getJSONObject(0))
    msg shouldBe a [Message]
  }
}

