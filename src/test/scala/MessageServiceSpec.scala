package com.chucho.wireapp.domain
/**
  * Created by chucho on 12/13/16.
  */
import java.io.File

import org.scalatest.AsyncFlatSpec
import org.scalatest._
import com.chucho.wireapp.system.{ClientHTTP, FileSystemImpl}


class MessageServiceSpec extends AsyncFlatSpec with BeforeAndAfterEach{

  val fileSystem = new FileSystemImpl(new File("/tmp"))
  val network = new ClientHTTP
  val messageService = new MessageServiceImpl(fileSystem,network)

  override def beforeEach(): Unit ={
    fileSystem.getFiles
      .filter(_.getName.endsWith(".json"))
      .foreach(_.delete())
  }


  behavior of "messasges"

  it should "eventually get 50 messages for page 0" in {
    val futureMessages = messageService.messagesFrom(0)
    futureMessages map { ms => assert( ms.size == 50) }
  }


  it should "give me 10 messages from page 1" in {
    messageService.messagesFrom(1)
      .map(_.take(1))
      .flatMap( xs => messageService.messagesFrom(1,10,xs.head.time) )
      .map { ms => assert( ms.size == 10) }
  }

  it should "give me 0 messages because there is no more messages for page 1" in {
    messageService.messagesFrom(1)
      .map( xs => xs.drop(49) )
      .flatMap( xs => messageService.messagesFrom(1,10,xs.head.time) )
      .map { ms => assert( ms.isEmpty) }
  }

  it should "can delete from cache file system 1 message from 50 remaining 49" in {
    val futureMessages = messageService.messagesFrom(0)
    futureMessages map  {
      ms => {
        assert( ms.size == 50)
        ms.head
      }
    } flatMap {
      msgToDelete =>
        messageService.deleteMessage(msgToDelete)
    } flatMap {
      _ =>  messageService.messagesFrom(0)
    } map {
      ms => assert( ms.size == 49)
    }
  }
}
