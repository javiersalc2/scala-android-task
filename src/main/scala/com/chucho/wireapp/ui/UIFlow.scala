package com.chucho.wireapp.ui

import android.app.Activity

import scala.concurrent.Future
/**
  * Created by chucho on 12/9/16.
  */

trait UIFlow[M,A,AC <: Activity] {
  def commandRunner(activity:AC,command:Future[A],dispatch:A => Unit):Unit
  def dispatch(activity:AC, action:A, model:M):Unit
  def dispatch(activity:AC,model:M):A => Unit
  def update(action:A,model:M):(M,Future[A])
  def render(activity:AC,model:M,dispatch:A => Unit):Unit
}

