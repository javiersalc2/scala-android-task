package com.chucho.wireapp.ui

import com.chucho.wireapp.domain.Message

/**
  * Created by chucho on 12/9/16.
  */
case class UIModel(page:Int,messages: List[Message],index:Int = 0)

