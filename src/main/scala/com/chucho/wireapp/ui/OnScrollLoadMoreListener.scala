package com.chucho.wireapp.ui

import android.widget.AbsListView

/**
  * Created by chucho on 12/10/16.
  */
class OnScrollLoadMoreListener(dispatch: Action => Unit,
                               currentPage: Int)
  extends AbsListView.OnScrollListener{

  override def onScrollStateChanged(view: AbsListView, scrollState: Int): Unit = ()

  override def onScroll(view: AbsListView,
                        firstVisibleItem: Int,
                        visibleItemCount: Int,
                        totalItemCount: Int): Unit = {
    if(firstVisibleItem + visibleItemCount == totalItemCount-2){
      dispatch( GiveMessage(currentPage + 1, firstVisibleItem + visibleItemCount) )
    }
  }
}