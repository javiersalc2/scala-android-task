package com.chucho.wireapp.ui

import java.text.SimpleDateFormat
import java.util.Date

import android.content.Context
import android.graphics.BitmapFactory
import android.view.View.OnTouchListener
import android.view.{LayoutInflater, View, ViewGroup}
import android.widget.{ArrayAdapter, ImageView, TextView}
import com.chucho.wireapp.domain.Message
import com.chucho.wireapp.system.CacheMap
import com.chucho.wireapp.{MainActivity, R, TR, TypedViewHolder}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

/**
  * Created by chucho on 12/10/16.
  */
class MessageArrayAdapter(activity: MainActivity,
                          resource: Int,
                          messages: List[Message],
                          listener: OnTouchListener)(implicit context: ExecutionContext)
  extends ArrayAdapter[Message](activity, resource, messages.asJava) {

  override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val msg = messages(position)
    val lInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
      .asInstanceOf[LayoutInflater]
    if (convertView == null) {
      val messageView: TypedViewHolder.message_text =
        TypedViewHolder.inflate(lInflater, TR.layout.message_text, parent, false)

      if (msg.text.startsWith("http")) {
        messageView.image_msg.setVisibility(View.VISIBLE)
        downloadImageInBackground(msg.text, messageView.image_msg)
      } else {
        messageView.text_msg.setText(msg.text)
        messageView.text_msg.setVisibility(View.VISIBLE)
      }
      messageView.time_txt.setText(hourFromTime(msg.time))
      messageView.rootView.setOnTouchListener(listener)
      messageView.rootView
    } else {
      val msgTxt = convertView.findViewById(R.id.text_msg).asInstanceOf[TextView]
      val imgMsg = convertView.findViewById(R.id.image_msg).asInstanceOf[ImageView]
      if (msg.text.startsWith("http")) {
        imgMsg.setVisibility(View.VISIBLE)
        downloadImageInBackground(msg.text, imgMsg)
        msgTxt.setVisibility(View.GONE)
      } else {
        msgTxt.setText(msg.text)
        msgTxt.setVisibility(View.VISIBLE)
        imgMsg.setVisibility(View.GONE)
      }
      convertView
    }
  }

  private def downloadImageInBackground(url: String, imageView: ImageView)
                                       (implicit context: ExecutionContext): Unit =
    CacheMap.getBitmapFromMemCache(url) match {
      case Some(bit) => imageView.setImageBitmap(bit)

      case None if !CacheMap.isBeingDownloaded(url) =>
        CacheMap.setIsBeingDownloaded(url)
        Future(activity.clientHttp.getResponseBytes(url))
          .onComplete {
            case Success(Success(bytes)) =>
              val options = new BitmapFactory.Options()
              options.inSampleSize = 8
              val bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options)
              CacheMap.addBitmapToMemoryCache(url, bmp)
              activity.runOnUiThread(new Runnable {
                override def run() = {
                  imageView.setImageBitmap(bmp)
                }
              })
            case _ =>
          }

      case None => imageView.setImageResource(activity.getResources
        .getIdentifier("com.chucho.wireapp:drawable/" +
          "photo_blank", null, null))
    }


  private def hourFromTime(time: Long): String =
    new SimpleDateFormat("HH:mm:ss").format(new Date(time))


}


