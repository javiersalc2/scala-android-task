package com.chucho.wireapp.ui

import android.app.Activity
import android.view.{MotionEvent, View, ViewConfiguration}
import android.widget.ListView

/**
  * Created by chucho on 12/10/16.
  */
class SwipeItemDeleteListener(
                               activity: Activity,
                               mListView: ListView,
                               dispatch: (Action) => Unit)
  extends View.OnTouchListener {

  val MoveDuration = 150
  val SwipeDuration = 250
  private var mDownX: Float = 0
  private var mSwipeSlop: Int = -1
  private var mItemPressed: Boolean = false
  private var mSwiping: Boolean = false

  override def onTouch(v: View, event: MotionEvent): Boolean = {
    if (mSwipeSlop < 0) {
      mSwipeSlop = ViewConfiguration.get(activity).
        getScaledTouchSlop
    }
    event.getAction match {
      case MotionEvent.ACTION_DOWN =>
        if (mItemPressed) {
          false
        }
        else {
          mItemPressed = true
          mDownX = event.getX()
          true
        }
      case MotionEvent.ACTION_CANCEL =>
        v.setAlpha(1)
        v.setTranslationX(0)
        mItemPressed = false
        true
      case MotionEvent.ACTION_MOVE => {
        val x = event.getX() + v.getTranslationX
        val deltaX = x - mDownX
        val deltaXAbs = Math.abs(deltaX)
        if (!mSwiping) {
          if (deltaXAbs > mSwipeSlop) {
            mSwiping = true
            mListView.requestDisallowInterceptTouchEvent(true)
          }
        }
        if (mSwiping) {
          v.setTranslationX(x - mDownX)
          v.setAlpha(1 - deltaXAbs / v.getWidth)
        }
      }
        true

      case MotionEvent.ACTION_UP =>
        if (mSwiping) {
          val x = event.getX() + v.getTranslationX
          val deltaX = x - mDownX
          val deltaXAbs = Math.abs(deltaX)
          var fractionCovered: Float = 0
          var endX: Float = 0
          var endAlpha: Float = 0
          val remove = if (deltaXAbs > v.getWidth / 4) {
            fractionCovered = deltaXAbs / v.getWidth
            endX = if (deltaX < 0) -v.getWidth else v.getWidth
            endAlpha = 0
            true
          } else {
            fractionCovered = 1 - (deltaXAbs / v.getWidth)
            endX = 0
            endAlpha = 1
            false
          }
          val duration = (1 - fractionCovered) * SwipeDuration
          mListView.setEnabled(false)
          v.animate().setDuration(duration.toLong)
            .alpha(endAlpha).
            translationX(endX)
            .withEndAction(new Runnable {
              def run(): Unit = {
                v.setAlpha(1)
                v.setTranslationX(0)
                if (remove) {
                  val position = mListView.getPositionForView(v)
                  dispatch(DeleteMessage(position, mListView.getFirstVisiblePosition))
                } else {
                  mSwiping = false
                  mListView.setEnabled(true)
                }
              }
            })
        }
        mItemPressed = false
        true
      case _ => false
    }
  }
}