package com.chucho.wireapp.ui

import com.chucho.wireapp.domain.Message

/**
  * Created by chucho on 12/9/16.
  */
sealed trait Action
case object NoAction extends Action
case class GiveMessage(page:Int,withIndex:Int = 0) extends Action
case class TakeMessage(page:Int,messages:List[Message],withIndex:Int = 0) extends Action
case class DeleteMessage(position:Int, withIndex:Int = 0) extends Action

