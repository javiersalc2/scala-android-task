package com.chucho.wireapp.system

import android.graphics.{Bitmap}
import android.util.LruCache
/**
  * Created by chucho on 12/18/16.
  */
trait CacheBitmap{
  def addBitmapToMemoryCache(key:String , bitmap:Bitmap ):Unit
  def getBitmapFromMemCache(key:String):Option[Bitmap]
}

object CacheMap extends CacheBitmap{
  val maxMemory = (Runtime.getRuntime.maxMemory / 1024).toInt
  val cacheSize = maxMemory / 8
  private var downloading:Set[String] = Set.empty


  private val mMemoryCache = new LruCache[String, Bitmap](cacheSize) {
    override def sizeOf(key:String ,bitmap:Bitmap):Int =
      bitmap.getByteCount / 1024
  }

  override def addBitmapToMemoryCache(key:String , bitmap:Bitmap ):Unit = {
    if (getBitmapFromMemCache(key).isEmpty) {
      mMemoryCache.put(key, bitmap)
    }
  }

  override def getBitmapFromMemCache(key:String):Option[Bitmap] =
    Option(mMemoryCache.get(key))

  def setIsBeingDownloaded(url:String):Unit =
    downloading = downloading + url

  def isBeingDownloaded(url:String):Boolean = downloading.contains(url)
}

