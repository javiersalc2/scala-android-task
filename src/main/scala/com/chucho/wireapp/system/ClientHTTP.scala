package com.chucho.wireapp.system

import java.net.{HttpURLConnection, URL}
import scala.util.Try

/**
  * Created by chucho on 12/9/16.
  */
class ClientHTTP extends Network {

  override def get(url: String): Try[String] =
    getResponseBytes(url).map(bytes => new String(bytes) )

  override def getResponseBytes(url: String): Try[Array[Byte]] = {
    Try {
      val is = new URL(url).openConnection
        .asInstanceOf[HttpURLConnection]
        .getInputStream
      Stream.continually(is.read)
        .takeWhile(_ != -1)
        .map(_.toByte).toArray
    }
  }

}

trait Network {
  def get(url:String):Try[String]
  def getResponseBytes(url: String): Try[Array[Byte]]
}
