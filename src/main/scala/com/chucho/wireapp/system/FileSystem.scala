package com.chucho.wireapp.system

import java.io.{BufferedOutputStream, File, FileOutputStream}
import scala.concurrent.ExecutionContext

/**
  * Created by chucho on 12/11/16.
  */
trait FileSystem {
  def saveFile(content:Array[Byte], fileName:String):Unit
  def readFile(fileName:String):Option[File]
  def getFiles:List[File]
}

class FileSystemImpl(internalDir:File)(implicit execution:ExecutionContext) extends FileSystem{
 private val absolutePath = internalDir.getAbsolutePath+File.separator

  override def saveFile(content:Array[Byte], fileName:String):Unit= {
      val bOut = new BufferedOutputStream(new FileOutputStream(absolutePath+fileName))
      Stream.continually(bOut.write(content))
      bOut.close
    }

  override def readFile(fileName: String):Option[File] = {
    val file = new File(absolutePath+fileName)
    if(file.exists()) Some(file)
    else None
  }
  override def getFiles:List[File] = new File(absolutePath).listFiles.toList

}