package com.chucho.wireapp.domain

import org.json.{JSONArray, JSONObject}
import scala.collection.JavaConverters._

/**
  * Created by chucho on 12/10/16.
  */
object Util {
  def parseResponse(jsonString: String): JSONArray = new JSONArray(jsonString)

  def jsonArrayToMessages(jsonArray:JSONArray):List[Message] = {
    val length = jsonArray.length
    (for (x <- 0 until length)
      yield jsonArray.getJSONObject(x))
      .map( x => msgFromJson(x))
      .toList
  }

  def msgFromJson(json:JSONObject):Message = {
    Message(id = json.getString("id"),
      time = json.getLong("time"),
      text = json.getString("text")
    )
  }


  def toJson(instance: AnyRef):JSONObject = {
    val map:Map[String, Any] = (Map[String, Any]() /: instance.getClass.getDeclaredFields) { (a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(instance))
    }
    new JSONObject(map.asJava)
  }

  def toJsonArray(jsonList:List[JSONObject]):JSONArray = {
    new JSONArray(jsonList.asJava)
  }


}
