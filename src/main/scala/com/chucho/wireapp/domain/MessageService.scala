package com.chucho.wireapp.domain

import java.io.FileInputStream

import com.chucho.wireapp.system.{FileSystem, Network}

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import scala.util.{Failure, Success}

/**
  * Created by chucho on 12/9/16.
  */


case class Message(id:String,time:Long,text:String)

trait MessageService {
  def messagesFrom(page: Int): Future[List[Message]]
  def messagesFrom(page: Int, qty: Int, timeOfLast: Long): Future[List[Message]]
  def deleteMessage(message: Message): Future[Unit]
}

class MessageServiceImpl(fileSystem: FileSystem,network:Network)(implicit context: ExecutionContext) extends MessageService {
  private val url = (num: Int) =>
    s"""
       |https://rawgit.com/wireapp/android_test_app/master/endpoint/$num.json
    """.stripMargin

  def messagesFrom(page: Int, qty: Int, timeOfLast: Long): Future[List[Message]] =
    messagesFrom(page).map(_.filter(_.time > timeOfLast).take(qty))

  override def messagesFrom(page: Int): Future[List[Message]] =
    Future {
      val fileName = s"$page.json"
      fileSystem.readFile(fileName) match {
        case Some(f) =>
          val jsonRaw = Source.fromInputStream(new FileInputStream(f)).mkString
          val jsonArr = Util.parseResponse(jsonRaw)
          Util.jsonArrayToMessages(jsonArr)
        case None =>
          network.getResponseBytes(url(page))
            .map { bytes =>
              val stringResponse = new String(bytes)
              fileSystem.saveFile(stringResponse.getBytes, fileName)
              Util.parseResponse(stringResponse)
            }
          match {
            case Success(jsonArr) => Util.jsonArrayToMessages(jsonArr)
            case Failure(_) => Nil
          }

      }
    }

  override def deleteMessage(message: Message): Future[Unit] =
    Future {
      fileSystem.getFiles
        .filter(_.isFile)
        .filter(_.getName.endsWith(".json"))
        .map(f => (f.getName, Source.fromInputStream(new FileInputStream(f)).mkString))
        .map(tup => (tup._1, Util.parseResponse(tup._2)))
        .map(tup => (tup._1, Util.jsonArrayToMessages(tup._2)))
        .collect {
          case tup@(_, ms) if ms.map(_.id).contains(message.id) => tup
        }.foreach { tup =>
        val (fileName, messages) = tup
        val messagesJson = messages.filter(m => m.id != message.id)
          .map(Util.toJson(_))
        val messagesLessDeleted = Util.toJsonArray(messagesJson).toString
        fileSystem.saveFile(messagesLessDeleted.getBytes, fileName)
      }
    }


}
