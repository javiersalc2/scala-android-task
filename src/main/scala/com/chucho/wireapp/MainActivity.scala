package com.chucho.wireapp

import android.os.{AsyncTask, Bundle}
import android.support.v7.app.AppCompatActivity
import com.chucho.wireapp.domain.MessageServiceImpl
import com.chucho.wireapp.system.{ClientHTTP, FileSystemImpl}
import com.chucho.wireapp.ui._

import scala.concurrent.{ExecutionContext, Future}

class MainActivity extends AppCompatActivity with UIFlow[UIModel, Action, MainActivity] {

  private implicit val context = this
  private implicit val execContext: ExecutionContext =
    ExecutionContext.fromExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

  val clientHttp = new ClientHTTP
  lazy val fileSystem = new FileSystemImpl(getFilesDir)
  private lazy val messageService = new MessageServiceImpl(fileSystem,clientHttp)
  import messageService._

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    dispatch(this, GiveMessage(page = 0), UIModel(0, Nil))
  }

  override def dispatch(activity: MainActivity, action: Action, model: UIModel): Unit = {
    val (newModel, cmd) = update(action, model)
    val newDispatch = dispatch(activity, newModel)
    commandRunner(activity,cmd,newDispatch)
    render(activity, newModel, newDispatch)
  }

  override def commandRunner(activity:MainActivity, command: Future[Action], dispatch:Action => Unit): Unit =
    command.onSuccess {
    case NoAction =>
    case a => activity.runOnUiThread(new Runnable {
      override def run(): Unit = dispatch(a)
    })
  }

  override def dispatch(activity: MainActivity, model: UIModel): (Action) => Unit =
    (action) => dispatch(activity, action, model)

  override def update(action: Action, model: UIModel): (UIModel, Future[Action]) =
    action match {
      case NoAction => (model,Future.successful(NoAction))

      case TakeMessage(page, ms, index) =>
        (UIModel(page, ms, index), Future.successful(NoAction))

      case GiveMessage(page, index) =>
        (model, messagesFrom(page)
          .map {
            case Nil => NoAction
            case ms => TakeMessage(page, model.messages ++ ms, index)
          })

      case DeleteMessage( pos, index) =>
        val msgToBeDeleted = model.messages(pos)
        (model, deleteMessage( msgToBeDeleted )
          .map( _ => TakeMessage(model.page,
            model.messages.filter( _.id != msgToBeDeleted.id ),index)))
    }

  override def render(activity: MainActivity, model: UIModel, dispatch: (Action) => Unit): Unit = {
    val vh: TypedViewHolder.main = TypedViewHolder.setContentView(this, TR.layout.main)
    model match {
      case UIModel(0, Nil, _) =>
      case UIModel(page, ms, index) =>

        val swipeDeleteListener = new SwipeItemDeleteListener(activity,vh.list_view,dispatch)
        val scrollTouchListener = new OnScrollLoadMoreListener(dispatch,page)
        val arrayAdapter = new MessageArrayAdapter(activity,
          android.R.layout.simple_list_item_1,ms,swipeDeleteListener)

        vh.list_view.setAdapter(arrayAdapter)
        vh.list_view.setSelection( index )
        vh.list_view.setOnScrollListener(scrollTouchListener)
    }
  }
}




